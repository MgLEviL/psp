/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author mglevil
 */
public abstract class Model_generic {
    private String url = "";
    private String login = "";
    private String password = "";
    Connection conn;
    String status;
    

    public Model_generic(String url, String login, String password){
        try{
            conn = DriverManager.getConnection(url, login, password);
            this.status = "Yeah!";
        }
        catch(SQLException ex){
            this.status = ex.getMessage();
        }catch(Exception e){
            this.status = e.getMessage();
            
        }
    }
    
    public String getStatus(){
        return status;
    }

}
