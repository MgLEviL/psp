/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

/**
 *
 * @author mglevil
 */
public class Restaurants{
    private int idrest;
    private String name;
    private String dir;
    private String localidad;
    private String tlf;
    
    public Restaurants(int idrest, String name, String dir, String localidad,
            String tlf){
        this.idrest = idrest;
        this.name = name;
        this.localidad = localidad;
        this.dir = dir;
        this.tlf = tlf;
    }

    public int getIdrest() {
        return idrest;
    }

    public void setIdrest(int idrest) {
        this.idrest = idrest;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDir() {
        return dir;
    }

    public void setDir(String dir) {
        this.dir = dir;
    }

    public String getLocalidad() {
        return localidad;
    }

    public void setLocalidad(String localidad) {
        this.localidad = localidad;
    }

    public String getTlf() {
        return tlf;
    }

    public void setTlf(String tlf) {
        this.tlf = tlf;
    }
    
}
