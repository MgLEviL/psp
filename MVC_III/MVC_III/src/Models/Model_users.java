/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
/**
 *
 * @author user
 */
public class Model_users extends Model_generic {
    private String query = "select * from usuarios";
    private Statement stmt = null;  
    
    public Model_users(String url, String login, String password){
        super(url, login, password);        
    }
    
    public ArrayList<Users> getUsersToController() throws SQLException{     
        ArrayList<Users> usuarios = new ArrayList();
        try {          
            
            stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(query);

            while (rs.next()) {
                int id = rs.getInt("id");
                String name = rs.getString("name");
                String passwd = rs.getString("passwd");

                Users us = new Users(id, name, passwd);
                usuarios.add(us);
            }
            
        }catch (SQLException e ) {
            this.status = e.getMessage();
        }finally {
            if (stmt != null) { stmt.close(); }
        }
        return usuarios;
    }

    public String setQuery(String pquery) {
           
        try {
            stmt = conn.createStatement();
            stmt.executeUpdate(pquery);
        } catch (SQLException ex) {
            this.status = ex.getMessage();
        }
        return "";
    }

}
