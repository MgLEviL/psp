/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author user
 */
public class Model_restaurants extends Model_generic {
    private String query = "select * from restaurantes";
    private Statement stmt = null;  

    
    public Model_restaurants(String url, String login, String password){
        super(url, login, password);
    }
    

    public ArrayList<Restaurants> getRestaurantsToController()throws SQLException{
        ArrayList<Restaurants> restaurantes = new ArrayList();
        try {          
            stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery("select * from restaurantes");

            
            while (rs.next()) {
                int idrest = rs.getInt("idrest");
                String name = rs.getString("name");
                String dir = rs.getString("dir");
                String localidad = rs.getString("localidad");
                String tlf = rs.getString("tlf");
                
                Restaurants re = new Restaurants(idrest, name, dir, localidad, tlf);
                restaurantes.add(re);
            }
            
        }catch (SQLException e ) {
            this.status = e.getMessage();
        }finally {
            if (stmt != null) { stmt.close(); }
        }
        return restaurantes;
    }
    
    public String setQuery(String pquery) {
           
        try {
            stmt = conn.createStatement();
            stmt.executeUpdate(pquery);
        } catch (SQLException ex) {
            this.status = ex.getMessage();
        }
        return "";
    }
}
