/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import Models.Model_restaurants;
import Models.Restaurants;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author user
 */
public class Controller_restaurants {
    private Model_restaurants rest = null;
    
    public ArrayList<Restaurants> getRestaurantsToView(String url,String login,
            String password) throws SQLException{
        
        Model_restaurants re = new Model_restaurants(url, login, password);
        this.rest = re;
        ArrayList<Restaurants> restaurantes = new ArrayList();
        
        restaurantes = re.getRestaurantsToController();
        
        return restaurantes;
    }
    
    public String getQueryInsert(String pquery){
        
        return rest.setQuery(pquery);       
    }
    
}
