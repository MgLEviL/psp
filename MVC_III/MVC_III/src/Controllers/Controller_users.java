/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import Models.Model_users;
import Models.Users;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author user
 */
public class Controller_users {
    private String url;
    private String login;
    private String password;
    private Model_users us;
    //jdbc:mysql://localhost:3306/cristeat
    //root root
    
    public Controller_users(String url, String login, String password){
        this.url = url;
        this.login = login;
        this.password = password;
    }
   
    
    public String setConnectDB(){
        Model_users us = new Model_users(url, login, password);  
        //referenciar variable de clase
        this.us = us;
        return us.getStatus();
    }
    
    public ArrayList<Users> getUsersToView() throws SQLException{
        Model_users us = new Model_users(url, login, password);  
        ArrayList <Users> usuarios = new ArrayList();
        
        usuarios = us.getUsersToController(); 
        
        return usuarios;
    }   

    public String getUrl() {
        return url;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }
    
    public void getStatus(){
        us.getStatus();
    }
 
    public String getQueryInsert(String pquery){
        return us.setQuery(pquery);       
    }

}
